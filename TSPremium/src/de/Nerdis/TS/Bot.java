package de.Nerdis.TS;

import org.bukkit.entity.Player;

import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;

public class Bot {

	public static void BotStart(){
		final TS3Config config = new TS3Config();
		config.setHost(Main.getPlugin().getConfig().getString("TS3.host"));
		config.setQueryPort(Main.getPlugin().getConfig().getInt("TS3.port"));
		config.setLoginCredentials(Main.getPlugin().getConfig().getString("TS3.username"), Main.getPlugin().getConfig().getString("TS3.passwort"));
		
		final TS3Query query = new TS3Query(config);
		query.connect();

		Main.ts3api = query.getApi();
		Main.ts3api.selectVirtualServerById(1);
		Main.ts3api.setNickname(Main.getPlugin().getConfig().getString("TS3.Botname"));
	}
	
	public static void addGroup(Player p, String ClientId){
		Main.ts3api.addClientToServerGroup(Main.getPlugin().getConfig().getInt("TS3.groupId"), Main.ts3api.getClientByUId(ClientId).getDatabaseId());
		p.sendMessage("�9[�6TS3Bot�9] �aDu hast nun den Premium Rang im TS!");
	}
	
}
