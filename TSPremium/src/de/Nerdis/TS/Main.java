package de.Nerdis.TS;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.github.theholywaffle.teamspeak3.TS3Api;

public class Main extends JavaPlugin {

	public static Plugin plugin;
	public static TS3Api ts3api;

	@Override
	public void onEnable() {
		
		System.out.println("[TSPremium] Plugin loaded.");

		plugin = this;

		loadConfig();

		Bot.BotStart();

	}

	@Override
	public void onDisable() {

		System.out.println("[TSPremium] Plugin unloaded.");
		
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdlabel, String[] args) {
		if(sender instanceof Player){
			Player p = (Player)sender;
			
			if(cmdlabel.equalsIgnoreCase("tspremium")){
				if(p.hasPermission("TS3.Premium")){
					
					if(args.length == 0){
						p.sendMessage("�9[�6TS3Bot�9] �c/tspremium <TSID>");
					}
					
					if(args.length == 1){
						Bot.addGroup(p, args[0]);
					}
					
					
				}
			}

		} else {
			sender.sendMessage("[TS3] Das ist ein Spieler Command!");
		}

		return true;
	}

	public static Plugin getPlugin(){
		return plugin;
	}

	public void loadConfig(){
		FileConfiguration cfg = getConfig();
		cfg.options().copyDefaults(true);
		cfg.addDefault("TS3.host", "127.0.0.1");
		cfg.addDefault("TS3.port", 10011);
		cfg.addDefault("TS3.username", "serveradmin");
		cfg.addDefault("TS3.passwort", "root");
		cfg.addDefault("TS3.Botname", "TS3Bot");
		cfg.addDefault("TS3.groupId", 1337);

		saveConfig();
	}

}
